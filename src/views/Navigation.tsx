import { css, jsx } from "@emotion/core"
import Link from "next/link"
import { useRouter } from "next/router"
import { FunctionComponent } from "react"

export const navigationHeight = (multiplier = 1) => `${3 * multiplier}rem`

export const Navigation: FunctionComponent = () => {
  const router = useRouter()
  const currentSection = router.asPath.split("/")[1] || ""

  return (
    <nav
      css={css`
        position: absolute;
        height: ${navigationHeight()};
      `}
    >
      <Link href="/" passHref={true}>
        <a
          className={`button ${currentSection === "" ? "" : "pseudo"}`}
          css={css`
            font-weight: bold;
            margin-right: 20px;
          `}
        >
          ITD 2019
        </a>
      </Link>

      <Link href="/projects" passHref={true}>
        <a
          className={`button ${currentSection === "projects" ? "" : "pseudo"}`}
        >
          Projects
        </a>
      </Link>

      <Link href="/people" passHref={true}>
        <a className={`button ${currentSection === "people" ? "" : "pseudo"}`}>
          People
        </a>
      </Link>
    </nav>
  )
}

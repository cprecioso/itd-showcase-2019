import { jsx } from "@emotion/core"
import {
  faChevronCircleRight,
  faEnvelope
} from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import sortBy from "lodash/sortBy"
import { observer } from "mobx-react"
import { getIdentifier, getParentOfType } from "mobx-state-tree"
import Link from "next/link"
import React, { FunctionComponent } from "react"
import { ITD, Person } from "../data/models"

export const PersonView = observer<
  FunctionComponent<{
    data: Person
    showProjects?: boolean
    showEmail?: boolean
  }>
>(({ data: person, showProjects = true, showEmail = true }) => {
  const involvedProjects = showProjects
    ? sortBy(
        getParentOfType(person, ITD).projects.filter(project =>
          [...project.students, ...project.coaches].includes(person)
        ),
        proj => proj.name.toLowerCase()
      )
    : []

  return (
    <div>
      <p>
        {person.firstName} <strong>{person.lastName}</strong>
        {showEmail ? (
          <React.Fragment>
            {" "}
            <a
              title={`Send ${person.firstName} an email`}
              href={`mailto:${person.email}`}
            >
              <FontAwesomeIcon size="xs" icon={faEnvelope} />
            </a>
          </React.Fragment>
        ) : null}
        {involvedProjects.length === 1 ? (
          <React.Fragment>
            {" "}
            <Link
              href="/projects/[groupId]"
              as={`/projects/${getIdentifier(involvedProjects[0])!}`}
              passHref={true}
            >
              <a title={`View project "${involvedProjects[0].name}"`}>
                <FontAwesomeIcon size="xs" icon={faChevronCircleRight} />
              </a>
            </Link>
          </React.Fragment>
        ) : null}
        {}
      </p>
      {involvedProjects.length > 1 ? (
        <ul>
          {involvedProjects.map(project => (
            <li key={getIdentifier(project)!}>
              <Link
                href="/projects/[groupId]"
                as={`/projects/${getIdentifier(project)!}`}
                passHref={true}
              >
                <a title={`View project "${project.name}"`}>
                  {project.name}{" "}
                  <FontAwesomeIcon size="xs" icon={faChevronCircleRight} />
                </a>
              </Link>
            </li>
          ))}
        </ul>
      ) : null}
    </div>
  )
})

export const PersonListView = observer<
  FunctionComponent<{
    data: Iterable<Person>
    showProjects?: boolean
    id?: string
  }>
>(({ data: personList, showProjects = true, id }) => {
  return (
    <ul>
      {sortBy([...personList], person => person.fullName.toLowerCase()).map(
        person => (
          <li
            id={id ? `${id}-${person.firstName.toLowerCase()}` : undefined}
            key={getIdentifier(person)!}
          >
            <PersonView data={person} showProjects={showProjects} />
          </li>
        )
      )}
    </ul>
  )
})

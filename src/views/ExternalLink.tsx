import { faExternalLinkAlt } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { FunctionComponent } from "react"

export const ExternalLink: FunctionComponent<
  JSX.IntrinsicElements["a"]
> = props => (
  <a target="_blank" rel="noopener noreferrer" {...props}>
    {props.children}&nbsp;
    <FontAwesomeIcon icon={faExternalLinkAlt} size="xs" />
  </a>
)

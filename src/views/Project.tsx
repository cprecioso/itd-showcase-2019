import { css, jsx } from "@emotion/core"
import { observer } from "mobx-react"
import Link from "next/link"
import React, { FunctionComponent } from "react"
import { Project } from "../data/models"
import { ExternalLink } from "./ExternalLink"
import { PersonListView, PersonView } from "./Person"
import { ProjectMaterialsView } from "./ProjectMaterials"
import { lBreakpoint } from "./responsive"

export const ProjectView = observer<FunctionComponent<{ data: Project }>>(
  ({ data: project }) => {
    const headerRef = React.useRef<HTMLHeadingElement>(null)
    return (
      <article>
        {project.materials.finalVideo !== null ? (
          <video
            className="full"
            css={css`
              object-fit: cover;
            `}
            src={project.materials.finalVideo.url}
            preload="auto"
            poster={
              project.materials.mainImage !== null
                ? project.materials.mainImage.url
                : undefined
            }
            controls
            autoPlay
            playsInline
          />
        ) : null}

        <div
          css={css`
            background: white;
            margin-top: 50px;
          `}
          className={`flex one three-${lBreakpoint}`}
        >
          <main className={`two-third-${lBreakpoint}`}>
            <header>
              <h1
                ref={headerRef}
                css={css`
                  padding: 0;
                `}
              >
                {project.name}
              </h1>
              <p
                css={css`
                  margin-top: 0;
                `}
              >
                {project.oneLineDescription}
              </p>
            </header>
          </main>

          <aside
            className={`third-${lBreakpoint}`}
            css={css`
              @media screen and (min-width: ${lBreakpoint}px) {
                border-left: 1px solid lightgray;
              }
            `}
          >
            <div
              css={css`
                h3,
                ul,
                li,
                p {
                  padding: 0;
                  margin: 0;
                  list-style: none;
                }
              `}
            >
              <h3>Students</h3>
              <PersonListView
                showProjects={false}
                data={project.students}
              ></PersonListView>
              <h3
                css={css`
                  margin-top: 1em !important;
                `}
              >
                Coach
              </h3>
              <Link
                href={`/people#coach-${project.coaches[0].firstName.toLowerCase()}`}
                passHref={true}
              >
                <a>
                  <PersonView
                    showEmail={false}
                    showProjects={false}
                    data={project.coaches[0]}
                  />
                </a>
              </Link>
            </div>

            <p>
              Group {project.groupName}
              <br />
              <ExternalLink
                href={`https://www.projectcamp.us/projects/${project.url}`}
              >
                projectcampus page
              </ExternalLink>
            </p>
          </aside>
        </div>
        <div
          css={css`
            margin-top: 50px;
          `}
        >
          <ProjectMaterialsView data={project.materials} />
        </div>
      </article>
    )
  }
)

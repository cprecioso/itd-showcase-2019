import { css } from "@emotion/core"

export const lBreakpoint = 800
export const mBreakpoint = 500

export const makeSquare = css`
  &:after {
    content: "";
    display: block;
    padding-bottom: 100%;
  }
`

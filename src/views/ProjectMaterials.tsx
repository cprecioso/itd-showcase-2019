import { css, jsx } from "@emotion/core"
import compact from "lodash/compact"
import { observer } from "mobx-react"
import { FunctionComponent } from "react"
import { File, ProjectMaterials } from "../data/models"
import { FileListView } from "./File"
import { lBreakpoint } from "./responsive"

const ProjectsMaterialRowView = observer<
  FunctionComponent<{
    name: string
    data: Iterable<File>
  }>
>(({ data: files, name }) => {
  return (
    <div className={`flex one four-${lBreakpoint}`}>
      <h4
        css={css`
          margin-bottom: 0;
          padding-bottom: 0.5em;
          margin-top: 0.2em;
          @media screen and (min-width: ${lBreakpoint}px) {
            margin-top: 2em;
            text-align: center;
          }
        `}
      >
        {name}
      </h4>

      <div className={`full three-fourth-${lBreakpoint}`}>
        <FileListView data={files} />
      </div>
    </div>
  )
})

export const ProjectMaterialsView = observer<
  FunctionComponent<{ data: ProjectMaterials }>
>(({ data: materials }) => {
  return (
    <div
      className="flex"
      uk-lightbox="animation: slide; video-autoplay: true; toggle: a.show-lightbox;"
    >
      {materials.videos.length > 0 ? (
        <ProjectsMaterialRowView name="Videos" data={materials.videos} />
      ) : null}

      {materials.images.length > 0 ? (
        <ProjectsMaterialRowView
          name="Images"
          data={
            materials.mainImage
              ? [materials.mainImage, ...materials.images]
              : materials.images
          }
        />
      ) : null}

      <ProjectsMaterialRowView
        name="Deliverables"
        data={compact([
          materials.conceptCard,
          materials.hardwareCard,
          materials.softwareCard
        ])}
      />

      {materials.extraMaterial.length > 0 ? (
        <ProjectsMaterialRowView
          name="Extra material"
          data={materials.extraMaterial}
        />
      ) : null}
    </div>
  )
})

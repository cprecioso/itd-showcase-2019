import { css, jsx } from "@emotion/core"
import { observer } from "mobx-react"
import { getIdentifier } from "mobx-state-tree"
import Link from "next/link"
import { FunctionComponent } from "react"
import { Project } from "../data/models"
import { lBreakpoint, mBreakpoint } from "./responsive"

export const ProjectCardView = observer<FunctionComponent<{ data: Project }>>(
  ({ data: project }) => (
    <article className="card">
      <div
        css={css`
          height: 180px;
          background-size: cover;
          background-repeat: no-repeat;
          background-position: center center;
        `}
        style={{
          backgroundImage: project.materials.mainImage
            ? `url("${project.materials.mainImage.thumbnailUrl}")`
            : undefined
        }}
      ></div>
      <header>
        <h3>{project.name}</h3>
      </header>

      <p
        css={css`
          font-size: 0.8em;
          font-weight: normal;
        `}
      >
        {project.oneLineDescription}
      </p>
    </article>
  )
)

export const ProjectCardListView = observer<
  FunctionComponent<{ data: Iterable<Project> }>
>(({ data: projectList }) => (
  <div className={`flex one two-${mBreakpoint} three-${lBreakpoint}`}>
    {[...projectList].map(project => {
      const projectId = getIdentifier(project)!
      return (
        <div key={projectId}>
          <Link
            href="/projects/[groupId]"
            as={`/projects/${projectId}`}
            passHref={true}
          >
            <a
              css={css`
                color: inherit;
                cursor: pointer;
              `}
              title={`Visit the page for ${project.name}`}
            >
              <ProjectCardView data={project} />
            </a>
          </Link>
        </div>
      )
    })}
  </div>
))

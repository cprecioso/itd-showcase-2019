import { css, jsx } from "@emotion/core"
import styled from "@emotion/styled"
import {
  faCube,
  faFileArchive,
  faMicrochip,
  IconDefinition
} from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import times from "lodash/times"
import { observer } from "mobx-react"
import { getIdentifier } from "mobx-state-tree"
import React, { FunctionComponent } from "react"
import { File } from "../data/models"
import { makeSquare, mBreakpoint } from "./responsive"

const makeRandomColor = () =>
  `rgb(${times(3, () => Math.floor(Math.random() * 360))})`

export const FileBaseView = styled.div<{ backgroundColor?: string }>`
  width: 100%;
  height: 100%;
  max-width: 200px;
  max-height: 200px;
  ${makeSquare};

  margin: 5px;
  overflow: hidden;
  background: ${props => props.backgroundColor || "white"};
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  border-radius: 4px;

  box-shadow: 0 0 5px lightgray;
  transform: translate(0px, 0px);
  transition: box-shadow 100ms ease-in-out, transform 100ms ease-in-out;

  &:hover {
    box-shadow: 1px 3px 7px -1px lightgray;
    transform: translate(-1px, -3px);
  }
`

const NoPreviewFileView = observer<
  FunctionComponent<{ data: File; icon?: IconDefinition }>
>(({ data: file, icon }) => {
  return (
    <a
      href={file.url}
      css={css`
        color: inherit;
      `}
    >
      <FileBaseView backgroundColor={makeRandomColor()}>
        <div
          css={css`
            width: 100%;
            height: 100%;
            position: relative;
          `}
        >
          <p
            css={css`
              font-size: 1.5em;
              margin-top: 0;
              position: absolute;
              top: 5px;
              left: 5px;
            `}
          >
            {file.stem}
            <br />({file.extension})
          </p>
          {icon ? (
            <div
              css={css`
                position: absolute;
                bottom: 5px;
                right: 5px;
              `}
            >
              <FontAwesomeIcon icon={icon} size="3x" />
            </div>
          ) : null}
        </div>
      </FileBaseView>
    </a>
  )
})

const FallbackFileView = observer<FunctionComponent<{ data: File }>>(
  ({ data: file }) => {
    React.useMemo(
      () => console.error("Unrecognized material", file.extension),
      [file.extension]
    )
    return <NoPreviewFileView data={file} />
  }
)

const ImageFileView = observer<FunctionComponent<{ data: File }>>(
  ({ data: file }) => (
    <a href={file.url} className="show-lightbox" data-type="image">
      <FileBaseView
        css={css`
          background-position: center center;
          background-repeat: no-repeat;
          background-size: cover;
        `}
        style={{ backgroundImage: `url(${file.thumbnailUrl})` }}
      />
    </a>
  )
)

const VideoFileView = observer<FunctionComponent<{ data: File }>>(
  ({ data: file }) => (
    <a
      href={file.url}
      className="show-lightbox"
      data-type="video"
      data-poster={file.thumbnailUrl}
      data-alt={file.basename}
      data-caption={file.basename}
    >
      <FileBaseView
        css={css`
          background-position: center center;
          background-repeat: no-repeat;
          background-size: cover;
        `}
        style={{ backgroundImage: `url("${file.thumbnailUrl}")` }}
      />
    </a>
  )
)

const PDFFileView = observer<FunctionComponent<{ data: File }>>(
  ({ data: file }) => (
    <a
      href={file.url}
      className="show-lightbox"
      data-type="iframe"
      data-poster={file.thumbnailUrl}
      data-alt={file.basename}
      data-caption={file.basename}
    >
      <FileBaseView
        css={css`
          background-position: center center;
          background-repeat: no-repeat;
          background-size: cover;
        `}
        style={{ backgroundImage: `url("${file.thumbnailUrl}")` }}
      />
    </a>
  )
)

const InoFileView = observer<FunctionComponent<{ data: File }>>(
  ({ data: file }) => <NoPreviewFileView data={file} icon={faMicrochip} />
)

const SldPrtFileView = observer<FunctionComponent<{ data: File }>>(
  ({ data: file }) => <NoPreviewFileView data={file} icon={faCube} />
)

const ZipFileView = observer<FunctionComponent<{ data: File }>>(
  ({ data: file }) => <NoPreviewFileView data={file} icon={faFileArchive} />
)

export const FileView = observer<FunctionComponent<{ data: File }>>(
  ({ data: file }) => {
    switch (file.extension) {
      case "jpg":
      case "jpeg":
      case "gif":
      case "png":
        return <ImageFileView data={file} />
      case "mp4":
        return <VideoFileView data={file} />
      case "pdf":
        return <PDFFileView data={file} />
      case "ino":
        return <InoFileView data={file} />
      case "sldprt":
        return <SldPrtFileView data={file} />
      case "zip":
        return <ZipFileView data={file} />
      default:
        return <FallbackFileView data={file} />
    }
  }
)

export const FileListView = observer<
  FunctionComponent<{ data: Iterable<File> }>
>(({ data: files }) => {
  return (
    <div className={`flex three four-${mBreakpoint}`}>
      {[...files].map(file => (
        <FileView key={getIdentifier(file)!} data={file} />
      ))}
    </div>
  )
})

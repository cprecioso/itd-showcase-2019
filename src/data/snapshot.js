// @preval
// @ts-check

const globby = require("globby")
const _ = require("lodash")
const fse = require("fs-extra")
const path = require("path")

/** @type {import("mobx-state-tree").SnapshotIn<typeof import("./models").ITD>} */
const snapshot = require("./raw-snapshot.json")

const r = path.resolve.bind(path, __dirname, "../..")

/** @type {(relPath:string)=>null|(import("mobx-state-tree").SnapshotIn<typeof import("./models").NullableFile>)} */
const makeNullableFile = relPath => {
  const [file] = globby.sync(
    relPath,
    // @ts-ignore
    {
      cwd: r("static/materials"),
      onlyFiles: true,
      fs: fse
    }
  )

  return file
    ? {
        url: (path
          .resolve("/static/materials", file)
          .split("/")
          .map(encodeURIComponent)
          .join("/"))
      }
    : null
}

/** @type {(groupName:string,glob:string)=>import("mobx-state-tree").SnapshotIn<typeof import("./models").FileList>} */
const makeFileList = (groupName, glob) =>
  _(
    globby.sync(
      glob,
      // @ts-ignore
      {
        cwd: r("static/materials", groupName),
        onlyFiles: true,
        fs: fse
      }
    )
  )
    .sortBy(name => {
      const stem = path.basename(name, path.extname(name))
      const stemAsNumber = Number.parseInt(stem, 10)
      return (Number.isNaN(stemAsNumber)) ? stem : stemAsNumber
    })
    .map(file => makeNullableFile(`${groupName}/${file}`))
    .compact()
    .value()

for (const project of snapshot.projects || []) {
  project.materials = {
    conceptCard: makeNullableFile(`${project.groupName}/conceptCard.pdf`),
    finalVideo: makeNullableFile(`${project.groupName}/finalVideo.mp4`),
    softwareCard: makeNullableFile(`${project.groupName}/softwareCard.pdf`),
    hardwareCard: makeNullableFile(`${project.groupName}/hardwareCard.pdf`),
    images: makeFileList(project.groupName, "images/*"),
    videos: makeFileList(project.groupName, "video/*"),
    extraMaterial: makeFileList(project.groupName, "extraMaterial/*"),
    mainImage: makeNullableFile(`${project.groupName}/mainImage.*`)
  }
}

module.exports = snapshot

// @ts-ignore
if (module === require.main) {
  console.log(JSON.stringify(snapshot, null, 2))
}

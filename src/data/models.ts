import { Instance, types as t } from "mobx-state-tree"
import { basename, extname, relative, resolve } from "path"

export const File = t
  .model("File", {
    url: t.identifier
  })
  .views(self => ({
    get fileName() {
      return decodeURI(self.url)
    }
  }))
  .views(self => ({
    get basename() {
      return basename(self.fileName)
    },
    get extension() {
      return extname(self.fileName)
        .slice(1)
        .toLowerCase()
    },
    get stem() {
      const stem = basename(self.fileName, extname(self.fileName))
      switch (stem) {
        case "conceptCard":
          return "Concept card"
        case "hardwareCard":
          return "Hardware card"
        case "softwareCard":
          return "Software card"
        default:
          return stem
      }
    },
    get thumbnailUrl() {
      return (
        resolve(
          "/static/materials-thumbnails",
          relative("/static/materials", self.url)
        ) + ".jpg"
      )
    }
  }))

export type File = Instance<typeof File>

export const NullableFile = t.maybeNull(File)
export type NullableFile = Instance<typeof NullableFile>

export const FileList = t.array(File)
export type FileList = Instance<typeof FileList>

export const Person = t
  .model("Person", {
    firstName: t.string,
    lastName: t.string,
    email: t.identifier
  })
  .views(self => ({
    get fullName() {
      return self.firstName + " " + self.lastName
    }
  }))
export type Person = Instance<typeof Person>

const ProjectMaterials = t.model("ProjectMaterials", {
  conceptCard: NullableFile,
  images: FileList,
  videos: FileList,
  finalVideo: NullableFile,
  hardwareCard: NullableFile,
  softwareCard: NullableFile,
  extraMaterial: FileList,
  mainImage: NullableFile
})
export type ProjectMaterials = Instance<typeof ProjectMaterials>

export const Project = t.model("Group", {
  groupName: t.identifier,
  students: t.array(t.reference(Person)),
  coaches: t.array(t.reference(Person)),
  name: t.string,
  url: t.string,
  materials: ProjectMaterials,
  oneLineDescription: t.string
})
export type Project = Instance<typeof Project>

export const ProjectList = t.array(Project)
export type ProjectList = Instance<typeof ProjectList>

export const PersonList = t.array(Person)
export type PersonList = Instance<typeof PersonList>

export const ITD = t.model("ITD", {
  projects: t.array(Project),
  students: t.array(Person),
  coaches: t.array(Person)
})
export type ITD = Instance<typeof ITD>

import { ITD } from "./models"
import snapshot from "./snapshot"

const data = ITD.create(snapshot as any)
export default data

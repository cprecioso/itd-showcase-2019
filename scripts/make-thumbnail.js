// @ts-check

const sharp = require("sharp")
const { pipeline } = require("stream")
const { createWriteStream } = require("fs-extra")

let [, , _inputFile, _outputFile, _width, _height, _position] = process.argv

const width = Number.parseInt(_width, 10)
const height = Number.parseInt(_height, 10)

const position =
  _position === "top"
    ? sharp.gravity.north
    : _position === "center"
    ? sharp.gravity.center
    : sharp.strategy.attention

const setOptions = (/** @type {sharp.Sharp} */ transformer) =>
  transformer
    .trim()
    .resize(width, height, {
      fit: "cover",
      position,
      withoutEnlargement: true
    })
    .jpeg({ quality: 90, progressive: true })

if (_inputFile === "-") {
  pipeline(
    process.stdin,
    setOptions(sharp()),
    createWriteStream(_outputFile),
    err => {
      if (err) throw err
    }
  )
} else {
  setOptions(sharp(_inputFile)).toFile(_outputFile)
}

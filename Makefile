.PHONY: all
all: thumbnails out



MATERIALS_INPUT := static/materials
MATERIALS_OUTPUT := static/materials-thumbnails



image_files := $(sort $(wildcard $(MATERIALS_INPUT)/*/images/*) $(wildcard $(MATERIALS_INPUT)/*/*/*.jpg) $(wildcard $(MATERIALS_INPUT)/*/*/*.gif) $(wildcard $(MATERIALS_INPUT)/*/*/*.JPG) $(wildcard $(MATERIALS_INPUT)/*/*/*.jpeg) $(wildcard $(MATERIALS_INPUT)/*/*/*.png))
image_thumbnail_files := $(image_files:$(MATERIALS_INPUT)/%=$(MATERIALS_OUTPUT)/%.jpg)

$(image_thumbnail_files): $(MATERIALS_OUTPUT)/%.jpg: $(MATERIALS_INPUT)/%
	mkdir -p $(dir $@)
	node scripts/make-thumbnail.js $< $@ 200 200

.PHONY: images
images: $(image_thumbnail_files)



main_image_files := $(wildcard $(MATERIALS_INPUT)/*/mainImage.*)
main_image_thumbnail_files := $(main_image_files:$(MATERIALS_INPUT)/%=$(MATERIALS_OUTPUT)/%.jpg)

$(main_image_thumbnail_files): $(MATERIALS_OUTPUT)/%.jpg: $(MATERIALS_INPUT)/%
	mkdir -p $(dir $@)
	node scripts/make-thumbnail.js $< $@ 320 180

.PHONY: main_images
main_images: $(main_image_thumbnail_files)



video_files := $(wildcard $(MATERIALS_INPUT)/*/*.mp4) $(wildcard $(MATERIALS_INPUT)/*/*/*.mp4)
video_thumbnail_files := $(video_files:$(MATERIALS_INPUT)/%=$(MATERIALS_OUTPUT)/%.jpg)

$(video_thumbnail_files): $(MATERIALS_OUTPUT)/%.jpg: $(MATERIALS_INPUT)/% node_modules
	mkdir -p $(dir $@)
	$(shell node -e 'console.log(require("ffmpeg-static").path)') -y -i $< -an -frames:v 1 -ss 5 -f mjpeg pipe:1 | node scripts/make-thumbnail.js - $@ 200 200 center

.PHONY: videos
videos: $(video_thumbnail_files)



pdf_files := $(wildcard $(MATERIALS_INPUT)/*/*.pdf) $(wildcard $(MATERIALS_INPUT)/*/*/*.pdf)
pdf_image_files := $(pdf_files:$(MATERIALS_INPUT)/%=$(MATERIALS_OUTPUT)/%-intermediate.jpg)
pdf_thumbnail_files := $(pdf_files:$(MATERIALS_INPUT)/%=$(MATERIALS_OUTPUT)/%.jpg)

.INTERMEDIATE: $(pdf_image_files)
$(pdf_image_files): $(MATERIALS_OUTPUT)/%-intermediate.jpg: $(MATERIALS_INPUT)/%
	mkdir -p $(dir $@)
	sips -s format jpeg $< --out $@

$(pdf_thumbnail_files): $(MATERIALS_OUTPUT)/%.jpg: $(MATERIALS_OUTPUT)/%-intermediate.jpg
	mkdir -p $(dir $@)
	node scripts/make-thumbnail.js $< $@ 200 200 top

.PHONY: pdfs
pdfs: $(pdf_thumbnail_files)



.PHONY: thumbnails
thumbnails: images main_images videos pdfs



node_modules: yarn.lock package.json
	-rm -rf node_modules
	yarn

.next/BUILD_ID: node_modules pages/* src/* static/* types/*
	-rm -rf .next
	yarn next build

out: .next/BUILD_ID thumbnails
	-rm -rf out
	yarn next export

.PHONY: dev
dev: node_modules thumbnails
	yarn next dev

.PHONY: clean
clean:
	-rm -rf $(MATERIALS_OUTPUT) out .next

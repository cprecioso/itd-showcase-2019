interface NodeRequire {
  context(
    directory: string,
    includeSubdirs?: boolean,
    filter?: RegExp,
    mode?: "sync" | "eager" | "weak" | "lazy" | "lazy-once"
  ): WebpackRequireContext
}

interface WebpackRequireContext extends NodeRequireFunction {
  id: string
  resolve(id: string): string
  keys(): string[]
}

import { NextPage } from "next"
import data from "../src/data"
import { PersonListView } from "../src/views/Person"
import { lBreakpoint } from "../src/views/responsive"

const PeoplePage: NextPage = () => (
  <div className={`flex one two-${lBreakpoint}`}>
    <div>
      <h2>Students</h2>
      <PersonListView data={data.students} showProjects={true} />
    </div>
    <div>
      <h2>Coaches</h2>
      <PersonListView id="coach" data={data.coaches} showProjects={true} />
    </div>
  </div>
)

export default PeoplePage

import { css, jsx } from "@emotion/core"
import App from "next/app"
import "../src/global-styles.css"
import { Navigation, navigationHeight } from "../src/views/Navigation"

// @ts-ignore
if (process.browser) {
  require("uikit/dist/js/uikit")
}

class ITDApp extends App {
  render() {
    const { Component, pageProps } = this.props
    return (
      <div
        css={css`
          max-width: 1000px;
          box-sizing: content-box;
          padding: 0 20px;
          margin: ${navigationHeight(2)} auto;

          style {
            display: none !important;
          }
        `}
      >
        <Navigation />
        <Component {...pageProps} />
      </div>
    )
  }
}

export default ITDApp

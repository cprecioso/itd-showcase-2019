import { NextPage } from "next"
import Link from "next/link"

const IndexPage: NextPage = () => (
  <article>
    <h1>
      Interactive Technology Design
      <br />
      Spring 2019
    </h1>
    <p>
      Welcome! Here you can see your projects and the projects of your
      classmates. If you see something not right, you can send an email to{" "}
      <a href="mailto:c.o.preciosodomingo@student.tudelft.nl">Carlos</a> and he
      will fix it right away. Thanks for participating, and enjoy!
    </p>
    <p>
      <span className="label warning">Important!</span> Do check that everything
      in your project page is correct and fit for publishing, as the intention
      is to be able to show the results of the course publicly, and this is the
      data that we will use.
    </p>
    <p>
      Click on the{" "}
      <Link href="/projects" passHref={true}>
        Projects
      </Link>{" "}
      button above to see the experiences created for ITD 2019.
    </p>
  </article>
)
export default IndexPage

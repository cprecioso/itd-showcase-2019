import { resolveIdentifier } from "mobx-state-tree"
import { NextPage } from "next"
import data from "../../src/data"
import { Project } from "../../src/data/models"
import { ProjectView } from "../../src/views/Project"

const ProjectPage: NextPage<{ groupId: string }> = ({ groupId }) => {
  if (groupId) {
    const project = resolveIdentifier(Project, data, groupId)
    if (project) {
      return <ProjectView data={project} />
    }
  }
  return <p>Project not found</p>
}

ProjectPage.getInitialProps = async ctx => ({
  groupId: (ctx.query.groupId as string).toUpperCase()
})

export default ProjectPage

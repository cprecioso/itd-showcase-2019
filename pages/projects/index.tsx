import sortBy from "lodash/sortBy"
import { NextPage } from "next"
import data from "../../src/data"
import { ProjectCardListView } from "../../src/views/ProjectCard"

const ProjectsIndexPage: NextPage = () => (
  <ProjectCardListView
    data={sortBy(data.projects, proj => proj.name.toLowerCase())}
  />
)
export default ProjectsIndexPage

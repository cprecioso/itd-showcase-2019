// @ts-check

const withPlugins = require("next-compose-plugins")
const css = require("@zeit/next-css")
const _ = require("lodash")

/** @type{import("mobx-state-tree").SnapshotIn<typeof import("./src/data/models").ITD>} */
const snapshot = require("./src/data/snapshot")

module.exports = withPlugins([css], {
  exportPathMap: () => ({
    "/": { page: "/index" },
    "/projects/index": { page: "/projects/index" },
    "/people": { page: "/people" },
    ..._.fromPairs(
      snapshot.projects.map(project => {
        const groupId = project.groupName
        return [
          `/projects/${groupId}`,
          { page: "/projects/[groupId]", query: { groupId } }
        ]
      })
    )
  })
})
